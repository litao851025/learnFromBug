package com.geekhalo.demo;

import com.geekhalo.lego.core.enums.mvc.dict.EnumDictController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.IntStream;

@SpringBootApplication
@ComponentScan(value = {"com.geekhalo.lego.core.enums", "com.geekhalo.demo"})
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
