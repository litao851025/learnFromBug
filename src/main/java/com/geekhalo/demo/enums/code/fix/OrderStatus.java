package com.geekhalo.demo.enums.code.fix;

import lombok.Getter;

@Getter
public enum OrderStatus{
	CREATED("已创建"),
	TIMEOUT_CANCELLED("超时自动取消"),
	MANUAL_CANCELLED("手工取消"),
	PAID("已支付"),
	FINISHED("已完成");
	private final String description;

	OrderStatus(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * @return the description
	 * @return
	 */
	@Override
	public String toString() {
		return description;
	}

	public static void main(String... args){
		for (OrderStatus orderStatus : OrderStatus.values()){
			System.out.println(orderStatus.name() + ":" + orderStatus.toString());
		}
	}
}