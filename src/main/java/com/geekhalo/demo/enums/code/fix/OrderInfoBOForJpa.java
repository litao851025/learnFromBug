package com.geekhalo.demo.enums.code.fix;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "order_info_jpa")
public class OrderInfoBOForJpa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * 指定枚举的转换器
     */
//    @Convert(converter = CodeBasedOrderStatusConverter.class)
    private CodeBasedOrderStatus status;
}
