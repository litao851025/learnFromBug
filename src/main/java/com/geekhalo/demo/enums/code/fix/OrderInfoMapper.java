package com.geekhalo.demo.enums.code.fix;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface OrderInfoMapper {
    @Insert(value = "insert into order_info_mybatis (status) values (#{o.status})" )
    void save(@Param("o") OrderInfoBO orderInfo);
}
