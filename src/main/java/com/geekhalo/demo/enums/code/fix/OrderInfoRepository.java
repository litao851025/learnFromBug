package com.geekhalo.demo.enums.code.fix;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderInfoRepository extends JpaRepository<OrderInfoBOForJpa, Long> {
}
