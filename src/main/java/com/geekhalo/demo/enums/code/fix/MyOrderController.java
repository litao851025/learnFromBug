package com.geekhalo.demo.enums.code.fix;

import com.geekhalo.demo.enums.code.OrderVO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("enums/code/fix")
@RestController(value = "MyFixOrderController")
@Slf4j
public class MyOrderController {

    @GetMapping("myOrders")
    public List<OrderVO> myOrders(@RequestParam("status") CodeBasedOrderStatus status) {
        // 忽略逻辑
        log.info("status is {}", status);
        return null;
    }

    @PostMapping("myOrders")
    public List<OrderVO> myOrders(@RequestBody GetMyOrderRequest request)  {
        // 忽略逻辑
        log.info("status is {}", request.getStatus());
        return null;
    }

    @Data
    public static class GetMyOrderRequest{
        private CodeBasedOrderStatus status;
    }
}
