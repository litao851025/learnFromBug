package com.geekhalo.demo.enums.code.bug;

public enum OrderStatus{
	/**
	 * 已创建
	 */
	CREATED,

	/**
	 * 原来定义为 CANCELLED，当用户超时未支付时，系统自动取消该订单<br />
	 * 下次迭代将增加手工取消订单功能，手工取消订单状态改为 MANUAL_CANCELLED <br />
	 */
	TIMEOUT_CANCELLED,
	/**
     * 已支付
     */
	PAID,
	/**
     * 已完成
     */
	FINISHED;
}