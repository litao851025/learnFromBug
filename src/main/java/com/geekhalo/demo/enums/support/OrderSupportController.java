package com.geekhalo.demo.enums.support;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@RestController
@RequestMapping("enums/support")
@Slf4j
public class OrderSupportController {

    @GetMapping("myOrders")
    public List<OrderVO> myOrders(@RequestParam("status") CommonOrderStatus status) {
        // 忽略逻辑
        log.info("status is {}", status);
        return LongStream.range(0, 5)
                .mapToObj( id ->{
                    OrderVO order = new OrderVO();
                    order.setId(id);
                    order.setStatus(status);
                    return order;
                }).collect(Collectors.toList());
    }

    @PostMapping("myOrders")
    public List<OrderVO> myOrders(@RequestBody GetMyOrderRequest request)  {
        // 忽略逻辑
        log.info("status is {}", request.getStatus());
        return LongStream.range(0, 5)
                .mapToObj( id ->{
                    OrderVO order = new OrderVO();
                    order.setId(id);
                    order.setStatus(request.getStatus());
                    return order;
                }).collect(Collectors.toList());
    }

    @Data
    public static class GetMyOrderRequest{
        private CommonOrderStatus status;
    }
}
