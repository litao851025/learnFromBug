package com.geekhalo.demo.enums.support;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "order_info_jpa")
public class OrderInfoBO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * 指定枚举的转换器
     */
    @Convert(converter = CommonOrderStatusAttributeConverter.class)
    private CommonOrderStatus status;
}