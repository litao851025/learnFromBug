package com.geekhalo.demo.enums.limit.bug;

import lombok.Data;

@Data
public class ProductTypes{
	public static final Integer CLAZZ = 1;
	public static final Integer BOOK = 2;
	// 其他类型
}