package com.geekhalo.demo.enums.limit.bug;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "CreateBugOrderRequest", description = "创建单个订单")
class CreateOrderRequest {
    @ApiModelProperty(value = "产品类型")
    private Integer productType;
    @ApiModelProperty(value = "产品id")
    private Integer productId;
    @ApiModelProperty(value = "数量")
    private Integer amount;
}
