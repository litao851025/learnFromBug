package com.geekhalo.demo.enums.limit.bug;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/enums/limit/bug/order")
@RestController("BugOrderController")
public class OrderController {
    @PostMapping("create")
    public void createOrder(@RequestBody CreateOrderRequest request) {
        return;
    }
}
