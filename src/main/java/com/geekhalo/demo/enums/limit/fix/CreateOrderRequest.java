package com.geekhalo.demo.enums.limit.fix;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "CreateFixOrderRequest", description = "创建单个订单")
class CreateOrderRequest {
    @ApiModelProperty(value = "产品类型")
    private ProductType productType;
    @ApiModelProperty(value = "产品id")
    private Integer productId;
    @ApiModelProperty(value = "数量")
    private Integer amount;
}
