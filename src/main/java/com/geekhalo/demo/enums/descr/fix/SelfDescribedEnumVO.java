package com.geekhalo.demo.enums.descr.fix;

import com.geekhalo.lego.common.enums.SelfDescribedEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
public class SelfDescribedEnumVO {
    @ApiModelProperty(notes = "Name")
    private final String name;

    @ApiModelProperty(notes = "描述")
    private final String desc;

    public static SelfDescribedEnumVO from(SelfDescribedEnum selfDescribedEnum){
        if (selfDescribedEnum == null){
            return null;
        }
        return new SelfDescribedEnumVO(selfDescribedEnum.getName(), selfDescribedEnum.getDescription());
    }

    public static List<SelfDescribedEnumVO> from(List<SelfDescribedEnum> commonEnums){
        if (CollectionUtils.isEmpty(commonEnums)){
            return Collections.emptyList();
        }
        return commonEnums.stream()
                .filter(Objects::nonNull)
                .map(SelfDescribedEnumVO::from)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
