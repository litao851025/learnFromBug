package com.geekhalo.demo.enums.descr.fix;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.geekhalo.demo.enums.code.fix.CodeBasedOrderStatus;
import com.geekhalo.lego.common.enums.CodeBasedEnum;
import com.geekhalo.lego.common.enums.CommonEnum;
import com.geekhalo.lego.common.enums.SelfDescribedEnum;
import com.geekhalo.lego.core.enums.mvc.CommonEnumVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class SelfDescribedEnumJacksonCustomizer {

//    @Bean
    public Jackson2ObjectMapperBuilderCustomizer commonEnumBuilderCustomizer(){
        return builder ->{
            // 注册自定义枚举序列化器
            builder.serializerByType(SelfDescribedEnum.class, new SelfDescribedEnumJsonSerializer());
        };
    }

    static class SelfDescribedEnumJsonSerializer extends JsonSerializer {

        @Override
        public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            SelfDescribedEnum selfDescribedEnum = (SelfDescribedEnum) o;
            SelfDescribedEnumVO selfDescribedEnumVO = SelfDescribedEnumVO.from(selfDescribedEnum);
            jsonGenerator.writeObject(selfDescribedEnumVO);
        }
    }
}
