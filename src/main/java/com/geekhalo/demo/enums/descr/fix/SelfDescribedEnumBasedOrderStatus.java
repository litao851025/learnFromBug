package com.geekhalo.demo.enums.descr.fix;

import com.geekhalo.lego.common.enums.SelfDescribedEnum;

public enum SelfDescribedEnumBasedOrderStatus implements SelfDescribedEnum {
    CREATED("待支付"),
    TIMEOUT_CANCELLED("超时取消"),
    MANUAL_CANCELLED("手工取消"),
    PAID("支付成功"),
    FINISHED("已完成");
    private final String description;

    SelfDescribedEnumBasedOrderStatus(String description) {
        this.description = description;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
