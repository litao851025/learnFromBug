package com.geekhalo.demo.enums.descr.bug;

public enum OrderStatus {
    CREATED(1),
    TIMEOUT_CANCELLED(2),
    MANUAL_CANCELLED(5),
    PAID(3),
    FINISHED(4);
    private final int code;

    OrderStatus(int code) {
        this.code = code;
    }

}
