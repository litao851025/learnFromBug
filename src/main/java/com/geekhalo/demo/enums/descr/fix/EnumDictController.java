package com.geekhalo.demo.enums.descr.fix;

import com.geekhalo.lego.common.enums.SelfDescribedEnum;
import com.geekhalo.lego.core.enums.mvc.CommonEnumVO;
import com.geekhalo.lego.core.web.RestResult;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags = "通用字典接口2")
@RestController("EnumDictController2")
@RequestMapping("/enums/descr/fix/")
@Slf4j
public class EnumDictController {
    private Map<String, List<SelfDescribedEnum>> enumDict = new HashMap<String, List<SelfDescribedEnum>>();

    public EnumDictController(){
        add("OrderStatus", SelfDescribedEnumBasedOrderStatus.values());
    }

    private void add(String type, SelfDescribedEnumBasedOrderStatus[] values) {
        this.enumDict.put(type, Arrays.asList(values));
    }

    /**
     * 获取所有字典信息
     * @return
     */
    @GetMapping("all")
    public RestResult<Map<String, List<SelfDescribedEnumVO>>> allEnums(){
        Map<String, List<SelfDescribedEnumVO>> dictVo = Maps.newHashMapWithExpectedSize(enumDict.size());
        for (Map.Entry<String, List<SelfDescribedEnum>> entry : enumDict.entrySet()){
            dictVo.put(entry.getKey(), SelfDescribedEnumVO.from(entry.getValue()));
        }
        return RestResult.success(dictVo);
    }

    /**
     * 获取支持的全部字典类型
     * @return
     */
    @GetMapping("types")
    public RestResult<List<String>> enumTypes(){
        return RestResult.success(Lists.newArrayList(enumDict.keySet()));
    }

    /**
     * 获取指定字典的全部值
     * @param type
     * @return
     */
    @GetMapping("/{type}")
    public RestResult<List<SelfDescribedEnumVO>> dictByType(@PathVariable("type") String type){
        List<SelfDescribedEnum> enums = enumDict.get(type);

        return RestResult.success(SelfDescribedEnumVO.from(enums));
    }
}
