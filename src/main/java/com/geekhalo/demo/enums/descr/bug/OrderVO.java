package com.geekhalo.demo.enums.descr.bug;

import lombok.Data;

@Data
public class OrderVO {
    private Long id;
    private OrderStatus status;
}
