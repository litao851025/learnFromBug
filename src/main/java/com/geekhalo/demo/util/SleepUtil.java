package com.geekhalo.demo.util;

import java.util.concurrent.TimeUnit;

public class SleepUtil {

    public static void sleep(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
