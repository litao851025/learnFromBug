package com.geekhalo.demo.thread.deadlock.bug;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.*;

@Service
public class GlobalExecuteService {
    private ExecutorService executorService;

    @PostConstruct
    public void init() {
        executorService = new ThreadPoolExecutor(4, 4,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(20),
                new BasicThreadFactory.Builder()
                        .namingPattern("global_thread-%d")
                        .build(),
                new ThreadPoolExecutor.AbortPolicy());
    }

    public <T> Future<T> submit(Callable<T> callable){
        return this.executorService.submit(callable);
    }

    public void submit(Runnable runnable){
        this.executorService.submit(runnable);
    }
}
