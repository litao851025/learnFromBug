package com.geekhalo.demo.thread.paralleltask;

import com.geekhalo.demo.util.SleepUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    public Product getById(Long id) {
        SleepUtil.sleep(1);
        return new Product(id, "小米电视");
    }

    @Data
    @AllArgsConstructor
    public static class Product{
        private Long id;
        private String name;
    }
}
