package com.geekhalo.demo.thread.paralleltask;

import com.geekhalo.demo.util.SleepUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

@Service
public class CouponService {

    public Coupon getById(Long id) {
        SleepUtil.sleep(1);
        return new Coupon(id, "5 元通用优惠券");
    }

    @Data
    @AllArgsConstructor
    public static class Coupon{
        private Long id;
        private String name;
    }
}
