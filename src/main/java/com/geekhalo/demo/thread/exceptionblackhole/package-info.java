package com.geekhalo.demo.thread.exceptionblackhole;
/**
 * 线程核心参数 ThreadFactory 常规配置 <br />
 * 1. 为线程池中的线程启个名字 <br />
 * 2. 设置异常处理器 <br />
 * 3. 后台线程设置为 daemon = ture <br />
 *
 * 示例如下：
 * new BasicThreadFactory.Builder()
 *                 .namingPattern("XXXX-Thread-%d")
 *                 .uncaughtExceptionHandler((t, e) -> log.error("Failed to run task", e))
 *                 .daemon(true)
 *                 .priority() 可选
 */