package com.geekhalo.demo.thread.parallelfun;

import java.util.stream.Stream;

public enum PromotionType {
    ACTIVITY(1), COUPON(2);
    private final int code;

    PromotionType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static PromotionType parallelParseByCode(int code) {
        return Stream.of(PromotionType.values())
                .parallel()
                .filter(type -> type.getCode() == code)
                .findFirst()
                .orElse(null);
    }

    public static PromotionType parseByCode(int code) {
        return Stream.of(PromotionType.values())
                .filter(type -> type.getCode() == code)
                .findFirst()
                .orElse(null);
    }
}
