package com.geekhalo.demo.thread.parallelfun.fix;

import com.geekhalo.demo.thread.parallelfun.*;
import com.geekhalo.lego.core.web.RestResult;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RestController
@RequestMapping("thread/parallelfun/fix")
@Slf4j
public class OrderParallelFunFixController {
    @GetMapping("getOrdersByUsers")
    public RestResult<List<OrderVO>> allOrderByUsers(@RequestParam List<Long> users){
        Stopwatch  stopwatch = Stopwatch.createStarted();
        List<Order> orders = getByUserId(users);
        log.info("get data from DB cost {} ms", stopwatch.stop().elapsed(TimeUnit.MILLISECONDS));

        stopwatch = Stopwatch.createStarted();
        List<OrderVO> orderVOS = orders.stream()
                .map(order -> OrderVO.apply(order))
                .collect(Collectors.toList());
        log.info("convert to OrderVO {} ms", stopwatch.stop().elapsed(TimeUnit.MILLISECONDS));
        return RestResult.success(orderVOS);
    }

    private List<Order> getByUserId(List<Long> users) {
        int sizePreUser = 10000;
        int statusSize = OrderStatus.values().length;
        int orderTypeSize = OrderType.values().length;
        int productTypeSize = ProductType.values().length;
        int promotionTypeSize = PromotionType.values().length;
        List<Order> orders = Lists.newArrayListWithCapacity(users.size() * sizePreUser);
        for (Long userId : users) {
            for (int j = 0; j < sizePreUser; j++) {
                Order order = new Order();
                order.setUserId(userId);
                order.setId(userId * j);
                order.setOrderStatus(RandomUtils.nextInt(1, statusSize));
                order.setOrderType(RandomUtils.nextInt(1, orderTypeSize));
                order.setProductType(RandomUtils.nextInt(1, productTypeSize));
                order.setPromotionType(RandomUtils.nextInt(1, promotionTypeSize));
                orders.add(order);
            }
        }
        return orders;
    }
}
