package com.geekhalo.demo.thread.parallelfun;

import lombok.Data;

import java.util.stream.Stream;

@Data
public class OrderVO {
    private Long id;
    private Long userId;
    private OrderStatus status;
    private OrderType orderType;
    private ProductType productType;
    private PromotionType promotionType;

    public static OrderVO applyByParallel(Order order){
        OrderVO orderVO = new OrderVO();
        orderVO.setId(order.getId());
        orderVO.setUserId(order.getUserId());

        orderVO.setStatus(OrderStatus.parallelParseByCode(order.getOrderStatus()));
        orderVO.setOrderType(OrderType.parallelParseByCode(order.getOrderType()));
        orderVO.setProductType(ProductType.parallelParseByCode(order.getProductType()));
        orderVO.setPromotionType(PromotionType.parallelParseByCode(order.getPromotionType()));
        return orderVO;
    }

    public static OrderVO apply(Order order){
        OrderVO orderVO = new OrderVO();
        orderVO.setId(order.getId());
        orderVO.setUserId(order.getUserId());

        orderVO.setStatus(OrderStatus.parseByCode(order.getOrderStatus()));
        orderVO.setOrderType(OrderType.parseByCode(order.getOrderType()));
        orderVO.setProductType(ProductType.parseByCode(order.getProductType()));
        orderVO.setPromotionType(PromotionType.parseByCode(order.getPromotionType()));

        return orderVO;
    }
}
