package com.geekhalo.demo.thread.parallelfun;

import lombok.Data;

@Data
public class Order{
    private Long id;
    private Long userId;
    private Integer orderStatus;
    private Integer orderType;
    private Integer productType;
    private Integer promotionType;
}