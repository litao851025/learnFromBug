package com.geekhalo.demo.thread.parallelfun;

import java.util.stream.Stream;

public enum OrderType {
    BOOK(1),
    CLAZZ(2),
    COUPON(3);
    private final  int code;

    OrderType(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public static OrderType parallelParseByCode(int code) {
        return Stream.of(values())
                .parallel()
                .filter(type -> type.getCode() == code)
                .findFirst()
                .orElse(null);
    }

    public static OrderType parseByCode(int code) {
        return Stream.of(values())
                .filter(type -> type.getCode() == code)
                .findFirst()
                .orElse(null);
    }

}
