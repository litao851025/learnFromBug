package com.geekhalo.demo.mq.event;

import com.geekhalo.demo.util.SleepUtil;
import lombok.SneakyThrows;

import java.net.SocketTimeoutException;

public abstract class BaseController {
    @SneakyThrows
    protected void sendSMSNotice(Order order) {
        if (order.getId() % 5 == 0){
            SleepUtil.sleep(10);
            throw new SocketTimeoutException();
        }
    }

    protected void updateOrder(Order order) {

    }

    protected Order findById(Long orderId) {
        return new Order(orderId);
    }

    protected void checkToke(String token) {

    }
}
