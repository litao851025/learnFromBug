package com.geekhalo.demo.mq.event.fix;

import com.geekhalo.demo.mq.event.Order;
import com.geekhalo.demo.util.SleepUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.net.SocketTimeoutException;
import java.util.concurrent.*;

@Component
@Slf4j
@RocketMQMessageListener(topic = "order_event", consumerGroup = "send-sms-notice-group")
public class SMSSendNoticeConsumer implements RocketMQListener<OrderPaidEvent> {

    @Override
    public void onMessage(OrderPaidEvent event) {
        sendSMSNotice(event.getOrder());
    }

    @SneakyThrows
    protected void sendSMSNotice(Order order) {
        if (order.getId() % 5 == 0){
            SleepUtil.sleep(10);
            throw new SocketTimeoutException();
        }
    }
}
