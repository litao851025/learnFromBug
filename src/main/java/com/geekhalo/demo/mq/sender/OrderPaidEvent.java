package com.geekhalo.demo.mq.sender;

import com.geekhalo.demo.mq.event.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderPaidEvent {
    private Order order;
}
