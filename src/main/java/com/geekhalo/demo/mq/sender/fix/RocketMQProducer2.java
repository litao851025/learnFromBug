package com.geekhalo.demo.mq.sender.fix;

import com.geekhalo.demo.mq.sender.OrderPaidEvent;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
public class RocketMQProducer2 {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    /**
     * 使用 @TransactionalEventListener 替代 @EventListener 监听订单支付事件，然后发送消息到 RocketMQ
     * @param event
     */
    @TransactionalEventListener
    public void handle(OrderPaidEvent event){
        rocketMQTemplate.convertAndSend("order_event", event);
    }
}
