package com.geekhalo.demo.mq.sender;

import lombok.Data;

@Data
public class Order {
    private Long id;

    public Order(Long id) {
        setId(id);
    }

    public void paySuccess() {

    }
}
