package com.geekhalo.demo.mq.sender;

import com.geekhalo.demo.mq.event.Order;

public interface OrderRepository {
    Order getById(String id);

    void update(Order order);
}
