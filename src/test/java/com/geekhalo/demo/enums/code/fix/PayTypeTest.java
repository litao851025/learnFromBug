package com.geekhalo.demo.enums.code.fix;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PayTypeTest {

    @Test
    void apply() {
        Assertions.assertTrue(PayType.WX_JS.apply(PayType.WX_JS));
        Assertions.assertFalse(PayType.WX_JS.apply(PayType.ZFB));

        Assertions.assertTrue(PayType.YIN_LIAN.apply(PayType.YIN_LIAN));
        Assertions.assertTrue(PayType.YIN_LIAN.apply(PayType.WX_JS));
        Assertions.assertTrue(PayType.YIN_LIAN.apply(PayType.ZFB));
    }
}