package com.geekhalo.demo.enums.code.fix;

public enum PayType {
    WX_JS, ZFB, YIN_LIAN{
        /**
         * 重新默认方法，同时匹配多个值
         * @param type
         * @return
         */
        @Override
        public boolean apply(PayType type){
            return super.apply(type) || (type == WX_JS || type == ZFB);
        }
    };

    /**
     * 默认等于他自身
     * @param type
     * @return
     */
    public boolean apply(PayType type) {
        return this.equals(type);
    }
}
