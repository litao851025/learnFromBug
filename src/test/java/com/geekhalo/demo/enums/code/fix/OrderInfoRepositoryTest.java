package com.geekhalo.demo.enums.code.fix;

import com.geekhalo.demo.DemoApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = DemoApplication.class)
public class OrderInfoRepositoryTest {
    @Autowired
    private OrderInfoRepository orderInfoRepository;

    @Test
    public void save() {
        OrderInfoBOForJpa orderInfo = new OrderInfoBOForJpa();
        orderInfo.setStatus(CodeBasedOrderStatus.CREATED);
        orderInfoRepository.save(orderInfo);
    }
}
